import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccueilComponent} from './accueil/accueil.component';
import {AccueilArtisteComponent} from './artistes/components/accueil-artiste/accueil-artiste.component';

const routes: Routes = [
  {path : '', redirectTo : 'artistes', pathMatch : 'full'},
  {path : 'artistes', loadChildren : './artistes/artistes.module#ArtistesModule'},
  {path : '**', redirectTo : 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash : true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
