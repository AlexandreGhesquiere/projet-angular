import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ArtistesRoutingModule} from './artistes-routing.module';
import {BlakapeComponent} from './components/blakape/blakape.component';
import {LaCurieuseComponent} from './components/la-curieuse/la-curieuse.component';
import {AmenInkComponent} from './components/amen-ink/amen-ink.component';
import {TomLaMureneComponent} from './components/tom-la-murene/tom-la-murene.component';
import {AccueilArtisteComponent} from './components/accueil-artiste/accueil-artiste.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ContactComponent } from './contact/contact.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ObservablesComponent } from './containers/observables/observables.component';
import { RudyComponent } from './components/rudy/rudy.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    BlakapeComponent,
    LaCurieuseComponent,
    AmenInkComponent,
    TomLaMureneComponent,
    AccueilArtisteComponent,
    NavbarComponent,
    ContactComponent,
    ObservablesComponent,
    RudyComponent,
    NotFoundComponent,
  ],
  imports: [
    CommonModule,
    CarouselModule.forRoot(),
    ArtistesRoutingModule
  ]
})
export class ArtistesModule { }
