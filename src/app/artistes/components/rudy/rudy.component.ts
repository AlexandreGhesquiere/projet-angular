import {Component, Input, OnInit} from '@angular/core';
import {interval} from 'rxjs';
import {takeWhile, tap, map, endWith} from 'rxjs/operators';

@Component({
  selector: 'ang-rudy',
  templateUrl: './rudy.component.html',
  styleUrls: ['./rudy.component.scss']
})
export class RudyComponent implements OnInit {

  @Input() test;
  @Input() startAt: number = 0;
  @Input() endAt: number = 200000;
  @Input() step: number = 0.5;
  @Input() delay: number = 10;
  @Input() digits: number = 2;
  currentValue;

  reset() {
    interval(this.delay).pipe(
      map(x => this.startAt + x * this.step),
      tap(x => console.log(x)),
      takeWhile(x => x < this.endAt),
      endWith(this.endAt)
    ).subscribe(x => this.currentValue = x);
  }



  ngOnInit() {
    console.log('test', this.test);
  }

}
