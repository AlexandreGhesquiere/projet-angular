import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RudyComponent } from './rudy.component';

describe('RudyComponent', () => {
  let component: RudyComponent;
  let fixture: ComponentFixture<RudyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RudyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RudyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
