import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlakapeComponent } from './blakape.component';

describe('BlakapeComponent', () => {
  let component: BlakapeComponent;
  let fixture: ComponentFixture<BlakapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlakapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlakapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
