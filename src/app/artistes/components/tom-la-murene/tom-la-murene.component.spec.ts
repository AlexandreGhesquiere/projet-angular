import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TomLaMureneComponent } from './tom-la-murene.component';

describe('TomLaMureneComponent', () => {
  let component: TomLaMureneComponent;
  let fixture: ComponentFixture<TomLaMureneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TomLaMureneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TomLaMureneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
