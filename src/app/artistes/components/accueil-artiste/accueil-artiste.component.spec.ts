import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilArtisteComponent } from './accueil-artiste.component';

describe('AccueilArtisteComponent', () => {
  let component: AccueilArtisteComponent;
  let fixture: ComponentFixture<AccueilArtisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilArtisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilArtisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
