import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmenInkComponent } from './amen-ink.component';

describe('AmenInkComponent', () => {
  let component: AmenInkComponent;
  let fixture: ComponentFixture<AmenInkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmenInkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmenInkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
