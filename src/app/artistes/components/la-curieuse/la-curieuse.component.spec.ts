import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaCurieuseComponent } from './la-curieuse.component';

describe('LaCurieuseComponent', () => {
  let component: LaCurieuseComponent;
  let fixture: ComponentFixture<LaCurieuseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaCurieuseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaCurieuseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
