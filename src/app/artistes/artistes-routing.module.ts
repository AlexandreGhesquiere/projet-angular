import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BlakapeComponent} from './components/blakape/blakape.component';
import {LaCurieuseComponent} from './components/la-curieuse/la-curieuse.component';
import {AmenInkComponent} from './components/amen-ink/amen-ink.component';
import {TomLaMureneComponent} from './components/tom-la-murene/tom-la-murene.component';
import {AccueilArtisteComponent} from './components/accueil-artiste/accueil-artiste.component';
import {ContactComponent} from './contact/contact.component';
import {RudyComponent} from './components/rudy/rudy.component';
import {NotFoundComponent} from './components/not-found/not-found.component';

const routes: Routes = [
  {path : '', component : AccueilArtisteComponent, pathMatch : 'full'},
  {path : 'blakape', component : BlakapeComponent},
  {path : 'lacurieuse', component : LaCurieuseComponent},
  {path : 'amenInk', component : AmenInkComponent},
  {path : 'tom', component : TomLaMureneComponent},
  {path : 'contact', component : ContactComponent},
  {path : 'rudy', component : RudyComponent},
  {path : '**', component : NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistesRoutingModule { }
